<?php
class MY_Controller extends CI_Controller {
    
	private $title = '';
    private $css_list = array();
    private $js_list = array();

    function __construct()
    {
        parent::__construct();
    }
    
    protected function render($view_name, $view_data = array())
    {
        if ($this->input->is_ajax_request() && $this->input->get('_type') == 'ajax'){
            return $this->output
                        ->set_content_type('application/json')
                        ->set_output(json_encode(array(
                            'status' => true,
                            'markup' => $this->load->view($view_name, $view_data, true),
                        )));
        } else {
			$layout['title'] = $this->title;
			$layout['js_list'] = '';
			$layout['css_list'] = '';
			foreach ($this->js_list as $v)
				$layout['js_list'] .= sprintf('<script type="text/javascript" src="%s"></script>', $v);
			foreach ($this->css_list as $v)
				$layout['css_list'] .= sprintf('<link type="text/css" rel="stylesheet" href="%s" >', $v);
			$id = $this->session->id;
			$store_id = $this->session->store_id;
			$layout['user'] = $this->User_model->getUser($id);
			$layout['store'] = $this->Stores_model->getStore($store_id);
			$content['header'] = $this->load->view('layout/header', $layout, true);
			$content['page'] = $this->load->view($view_name, $view_data, true);
			
			$this->load->view('layout/head', $layout);
			$this->load->view('layout/content', $content);
			$this->load->view('layout/footer', $layout);
        }
    }
	
	public function title($title) {
        $this->title = $title;
    }
    public function js($item) {
        $this->js_list[] = $item;
    }
    public function css($item) {
        $this->css_list[] = $item;
    }
    
    protected function send_json($data = array())
    {
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
    }
}