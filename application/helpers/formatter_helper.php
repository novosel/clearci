<?php
function array_has_duplicates($array,$ignore = null) {
	if(isset($ignore)) {
		foreach($array as $key=>$arr) {
			if($arr == $ignore)
				unset($array[$key]);
		}
	}
   return count(array_filter($array)) !== count(array_unique(array_filter($array)));
}

function array_get_dublicate($array) {
    foreach ($array as $key => $value) {
        if(empty($value)) {
            unset($array[$key]);
        }
    }
    return array_unique( array_diff_assoc( $array, array_unique( $array ) ) );;
}

function normalizeArray(array $array, $field = 'id')
{
    $tmp = array();
    foreach($array as $row){
        $tmp[] = $row[$field];
    }
    return $tmp;
}
function normalizeArrayWithKey(array $array, $key = 'id',$field = 'id')
{
    $tmp = array();
    foreach($array as $row){
        if ($field){            
            $tmp[$row[$key]] = $row[$field];
        } else {
            $tmp[$row[$key]] = $row;
        }
    }
    return $tmp;
}
function clearEmptyRows($row)
{
    if (empty(array_filter($row))){
        return false;
    }
    return true;
}