<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout {
    private $obj;
    private $projects_list;
    private $title = '';
    private $css_list = array();
    private $js_list = array();

    function Layout() {
        $this->obj =& get_instance();
    }

    function view($view, $data = null) {
        $layout['title'] = $this->title;
        $layout['js_list'] = '';
        $layout['css_list'] = '';
        foreach ($this->js_list as $v)
            $layout['js_list'] .= sprintf('<script type="text/javascript" src="%s"></script>', $v);
        foreach ($this->css_list as $v)
            $layout['css_list'] .= sprintf('<link type="text/css" rel="stylesheet" href="%s" >', $v);
        $content['header'] = $this->obj->load->view('layout/header', $layout, true);
        $content['menu'] = $this->obj->load->view('layout/menu', $layout, true);
        $content['page'] = $this->obj->load->view($view, $data, true);
		
        $this->obj->load->view('layout/head', $layout);
        $this->obj->load->view('layout/content', $content);
        $this->obj->load->view('layout/footer', $layout);
    }
    function title($title) {
        $this->title = $title;
    }
    function js($item) {
        $this->js_list[] = $item;
    }
    function css($item) {
        $this->css_list[] = $item;
    }
}
