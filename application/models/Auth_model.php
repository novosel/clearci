<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
	
	function registration($email,$password) {
		$user = $this->db->
			select()->from('users')->
			where(array('email'=>$email))->
			get()->row();
			
		if(!isset($user)) {
			$this->db->insert('users', array('email'=>$email,'password'=>md5($password)));
			$id = $this->db->insert_id();
			$user_session = array(
				'id'        => $id,
				'login'     => $email,
			);
			$this->session->set_userdata($user_session);
			return true;
		}
		return false;
	}
	
	function login($email,$password) {
		$user = $this->db->
			select()->from('users')->
			where(array('email'=>$email, 'password'=>md5($password)))->
			get()->row();
			
		if(isset($user)) {
			$user_session = array(
				'id'        => $user->id,
				'login'     => $user->login,
				'store_id'     => $user->last_store,
			);
			$this->session->set_userdata($user_session);
			return true;
		}
		return false;
	}
	
}
