<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
	
	function getUser($id) {
		$user = $this->db->where('id',$id)->get('users')->row();
		return $user;
	}
	
}
