<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
    }
	
	public function index() {
		$redirect = 'dashboard';
		$error_message = null;
		
		$this->load->library('form_validation');
		$this->load->model('auth_model');
			
		if(!empty($this->input->post())) {
			$result = null;
			if($this->input->post('registration')){
				$this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.email]',
					array(
						'required' => 'You have not provided %s.',
						'is_unique' => 'This %s already exists.'
					)
				);
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('password_check', 'Password Confirmation', 'min_length[6]|max_length[16]|matches[password]');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
			
				if ($this->form_validation->run() != false) {
					$email = $this->input->post('email');
					$password = $this->input->post('password');
					$result = $this->auth_model->registration($email,$password);
				}
			}
			else if($this->input->post('login')){
				$this->form_validation->set_rules('email', 'Email', 'required');
				$this->form_validation->set_rules('password', 'Password', 'min_length[6]|max_length[16]');
				if ($this->form_validation->run() != false) {
					$email = $this->input->post('email');
					$password = $this->input->post('password');
					$result = $this->auth_model->login($email,$password);
					if(!$result)
						$error_message = 'Incorrect login or password';
				}
			}
			if($result)
				redirect(base_url('dashboard'));
		}
		$this->load->view('auth', array('error_message'=>$error_message));
	}
	
	public function logout() {
		if($this->session->id != ''){
            session_destroy();
        }
        redirect('');
    }
}
